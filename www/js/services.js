angular.module('starter.services', [])

.factory('Data', function($http,$rootScope) {
  // Might use a resource here that returns a JSON array

  // Historiques
  var history =  $http({
      url: 'http://52.174.163.132:8091/api/getHistory/' ,
      method: "GET"
    }).then(function(response) {
      return response.data;
    }, function(error) {
      return error;
      console.log(error);
    });

  // Votants
  var votants =  $http({
      url: 'http://52.174.163.132:8091/api/getVotants/?limit=50' ,
      method: "GET"
    }).then(function(response) {
      return response.data;
    }, function(error) {
      console.log(error);
    });

  // One Full Votant
  var oneFullVotants = $http({
      url: 'http://52.174.163.132:8091/api/getVotantFull/' + $rootScope.idVotant ,
      method: "GET",
      params:{
        idVotant:$rootScope.idVotant
      }
    }).then(function(response) {
      return response.data;
    }, function(error) {
      console.log(error);
    });

  return {
    all: function() {
      return history;
    },
    votants: function() {
      return votants;
    },
    oneFullVotants: function() {
      return oneFullVotants;
    }
  };
});
