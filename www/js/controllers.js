angular.module('starter.controllers', [])


.controller('DashCtrl', function($scope,$http,$base64,SweetAlert) {
$scope.resultScan = '';
$scope.scan = function ()
{
    cordova.plugins.barcodeScanner.scan(
        function (result) {
            if(!result.cancelled)
            {
                if(result.format == "QR_CODE")
                {
                    console.log(result);
                    $scope.$apply(function () {
                        $scope.resultScan = result;
                    });


                    $scope.idToGet = $base64.decode(result.text);

                    $http({
                      url: 'http://52.174.163.132:8091/api/checkVotant/' + $scope.idToGet,
                      method: "GET",
                      params:{
                        idFacebook : $scope.idToGet
                      }
                    }).then(function(response) {
                      console.log(response);
                      if(response.data){
                        //Exist
                        $scope.votants = response.data;
                        SweetAlert.swal("" + $scope.votants.facebook.name, "", "success", "Retour");
                        $scope.saveHistory();
                      }else{
                        // Dont Exist
                        $scope.votants = null;
                        SweetAlert.swal("Introuvable", "", "error");
                      }
                    }, function(error) {
                      console.log(error);
                    });


                }
                else{
                  SweetAlert.swal("Introuvable", "", "error");
                }
            }
        },
        function (error) {
            alert("Scanning failed: " + error);
        }
   );
}


$scope.saveHistory = function(){

    $http({
      url: 'http://52.174.163.132:8091/api/history/' ,
      method: "POST",
      params:{
        options : 2,
        id_votants : $scope.votants.id_votants
      }
    }).then(function(response) {
      if(response.data){
        return response;
      }else{
        // Error
        return "Erreur";
      }
    }, function(error) {
      console.log(error);
    });
};

})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };

})

.controller('HistoryCtrl', function($scope, Data,$rootScope) {

  Data.all().then(function(result){
        $scope.history = result.data;


        $scope.getHistory = function(id_votants){
                console.log(id_votants);

        };


  });

  $scope.checkHistory = function(History){
    var date = new Date(History.datetime);
    var today = new Date();
    var yesterday = date.setDate(today.getDate() - 1);

    if(yesterday < date < today){
      return true;
    }
    else{
      return false;
    }
  };

})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  }
})

.controller('InvitesCtrl', function($scope,Data) {

    $scope.votants = {};

    Data.votants().then(function(result){
      $scope.votants = result.data;
      console.log($scope.votants);
    });

});
